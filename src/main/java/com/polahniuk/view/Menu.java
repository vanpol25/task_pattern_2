package com.polahniuk.view;

import com.polahniuk.model.*;
import com.polahniuk.model.state.StateType;
import com.polahniuk.model.state.Task;
import com.polahniuk.model.state.impl.DoneState;
import com.polahniuk.model.state.impl.InProgressState;
import org.apache.logging.log4j.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Main view of project. Show menu in console.
 */
public class Menu {

    private Logger log = LogManager.getLogger(Menu.class);
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private List<User> users;
    private List<Task> tasks;
    private User me = new User("Admin");

    {
        users = Arrays.asList(
                new User("Igor"),
                new User("Ivan")
        );

        tasks = Arrays.asList(
                new Task("Do1"),
                new Task("Do2"),
                new Task("Do3"),
                new Task("Do4")
        );

        tasks.get(0).setState(new DoneState());
        tasks.get(1).setState(new InProgressState());
        tasks.get(2).setState(new InProgressState());

        users.get(0).addTask(tasks.get(0));
        users.get(0).addTask(tasks.get(1));
        users.get(1).addTask(tasks.get(2));
    }

    /**
     * Creates methods in constructor due to interface{@link Printable}.
     */
    public Menu() {
        setMenu();
        methods = new LinkedHashMap<>();
        methods.put("1", this::showTasks);
        methods.put("2", this::addUser);
        methods.put("3", this::addTask);
        methods.put("4", this::takeTask);
        show();
    }

    private void takeTask() {
        List<Task> todoTasks = tasks.stream().filter((task) -> task.getStateType() == StateType.TODO).collect(Collectors.toList());
        System.out.println("Todo tasks");
        for (int i = 0; i < todoTasks.size(); i++) {
            System.out.println((i + 1) + tasks.get(i).toString());
        }
        System.out.println("Please, select menu task.");
        int taskNumber = sc.nextInt();
        Task task = todoTasks.get(taskNumber + 1);
        me.addTask(task);
        System.out.println("You take task " + taskNumber + ", name " + task);
    }

    private void showTasks() {
        String keyMenu;
        do {
            System.out.println("\nMENU:" +
                    "\n1 - Show \"to do\" tasks" +
                    "\n2 - Show \"in progress\" tasks" +
                    "\n3 - Show \"review\" tasks" +
                    "\n4 - Show \"done\" tasks" +
                    "\n5 - Show \"my\" tasks" +
                    "\nQ - Exit");
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            switch (keyMenu) {
                case "1": {
                    showTasksByType(StateType.TODO);
                    break;
                }
                case "2": {
                    showTasksByType(StateType.IN_PROGRESS);
                    break;
                }
                case "3": {
                    showTasksByType(StateType.REVIEW);
                    break;
                }
                case "4": {
                    showTasksByType(StateType.DONE);
                    break;
                }
                case "5:": {
                    showMyTasks();
                    break;
                }
                default: {
                    break;
                }

            }
        } while (keyMenu.equals("Q"));
    }

    private void showMyTasks() {
        System.out.println("My tasks");
        for (Task task : tasks) {
            if (task.getUser() == me) {
                System.out.println(task);
            }
        }
    }

    private void showTasksByType(StateType type) {
        System.out.println(type + " tasks");
        for (Task task : tasks) {
            if (task.getStateType() == type) {
                System.out.println(task);
            }
        }
    }

    private void addTask() {
        System.out.println("Enter name of new task:");
        String name = sc.nextLine();
        tasks.add(new Task(name));
    }

    private void addUser() {
        System.out.println("Enter name of new user:");
        String name = sc.nextLine();
        users.add(new User(name));
    }

    /**
     * Creating menu.
     */
    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Show methodsTask");
        menu.put("2", "2 - Add user");
        menu.put("3", "3 - Add task");
        menu.put("4", "4 - Take task");
        menu.put("Q", "Q - Exit");
    }

    /**
     * Show menu in console.
     */
    private void getMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    /**
     * Main method to work with user in console.
     */
    private void show() {
        String keyMenu;
        do {
            getMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception e) {
                log.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

}