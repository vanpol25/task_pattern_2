package com.polahniuk.model;

@FunctionalInterface
public interface Printable {
    void print() throws Exception;
}

