package com.polahniuk.model.state.impl;

import com.polahniuk.model.state.State;
import com.polahniuk.model.state.StateType;

public class DoneState implements State {

    private StateType type = StateType.DONE;

    public StateType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DoneState";
    }
}
