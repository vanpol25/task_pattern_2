package com.polahniuk.model.state.impl;

import com.polahniuk.model.state.StateType;
import com.polahniuk.model.state.Task;
import com.polahniuk.model.state.State;

public class TodoState implements State {

    private StateType type = StateType.TODO;

    public StateType getType() {
        return type;
    }

    @Override
    public void inProgress(Task task) {
        task.setState(new InProgressState());
    }

    @Override
    public String toString() {
        return "TodoState";
    }
}
