package com.polahniuk.model.state.impl;

import com.polahniuk.model.state.StateType;
import com.polahniuk.model.state.Task;
import com.polahniuk.model.state.State;

public class InProgressState implements State {

    private StateType type = StateType.IN_PROGRESS;

    public StateType getType() {
        return type;
    }

    @Override
    public void todo(Task task) {
        task.setState(new TodoState());
    }

    @Override
    public void review(Task task) {
        task.setState(new ReviewState());
    }

    @Override
    public String toString() {
        return "InProgressState";
    }
}
