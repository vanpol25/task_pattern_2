package com.polahniuk.model.state.impl;

import com.polahniuk.model.state.StateType;
import com.polahniuk.model.state.Task;
import com.polahniuk.model.state.State;

public class ReviewState implements State {

    private StateType type = StateType.REVIEW;

    public StateType getType() {
        return type;
    }

    @Override
    public void todo(Task task) {
        task.setState(new TodoState());
    }

    @Override
    public void inProgress(Task task) {
        task.setState(new InProgressState());
    }

    @Override
    public void done(Task task) {
        task.setState(new DoneState());
    }

    @Override
    public String toString() {
        return "ReviewState";
    }
}
