package com.polahniuk.model.state;

import com.polahniuk.model.User;
import com.polahniuk.model.state.impl.TodoState;

public class Task {

    private static int counter = 0;
    private int id;
    private State state;
    private String name;
    private User user;

    public Task(String name) {
        counter++;
        this.id = counter;
        this.name = name;
        this.state = new TodoState();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public StateType getStateType() {
        return state.getType();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void todoRequest() {
        state.todo(this);
    }

    public void inProgressRequest() {
        state.inProgress(this);
    }

    public void reviewRequest() {
        state.review(this);
    }

    public void doneRequest() {
        state.done(this);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", state=" + state +
                ", name='" + name + '\'' +
                ", user=" + user +
                '}';
    }
}
