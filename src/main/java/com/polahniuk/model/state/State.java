package com.polahniuk.model.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

    Logger log = LogManager.getLogger(State.class);

    default void todo(Task task){log.info("todo - is not allowed");}

    default void inProgress(Task task){log.info("inProgress - is not allowed");}

    default void review(Task task){log.info("review - is not allowed");}

    default void done(Task task){log.info("done - is not allowed");}

    StateType getType();

}
