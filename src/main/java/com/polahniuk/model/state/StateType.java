package com.polahniuk.model.state;

public enum StateType {
    TODO, IN_PROGRESS, REVIEW, DONE
}
