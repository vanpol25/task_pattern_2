package com.polahniuk.model;

import com.polahniuk.model.state.Task;

import java.util.ArrayList;
import java.util.List;

public class User {

    private String name;
    private List<Task> tasks = new ArrayList<>();

    public User(String name) {
        this.name = name;
    }

    public Task getTask(int n) {
        return tasks.get(n);
    }

    public void addTask(Task task) {
        task.setUser(this);
        tasks.add(task);
    }

    public void removeTask(Task task) {
        task.setUser(null);
        tasks.remove(task);
    }

    @Override
    public String toString() {
        return "User \"" + name + ".";
    }
}
